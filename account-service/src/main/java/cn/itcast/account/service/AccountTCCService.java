package cn.itcast.account.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

/**
 * @author edevp
 * @description
 * @create 2022-05-18 15:24
 **/
@LocalTCC
public interface AccountTCCService {

    /**
     * deduct try的接口
     *
     * @param userId             the account
     * @param money             the money
     * @return the boolean
     */
    @TwoPhaseBusinessAction(name = "deduct", commitMethod = "confirm", rollbackMethod = "cancel")
    void deduct(@BusinessActionContextParameter(paramName = "userId") String userId,
                           @BusinessActionContextParameter(paramName = "money") int money);

    /**
     * confirm boolean. 提交事务的接口
     *
     * @param actionContext the action context
     * @return the boolean
     */
    boolean confirm(BusinessActionContext actionContext);

    /**
     * cancel boolean. 回滚接口
     *
     * @param actionContext the action context
     * @return the boolean
     */
    boolean cancel(BusinessActionContext actionContext);

}
