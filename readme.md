# 删除苹果系统备份文件
    find . -name '._*.java' -type f -delete
    
    find . -name '._.*' -type f -delete
    
    find . -name '._*' -type f -delete
    
# 分支说明
## base 分支
    基本springcloud工程
## AT 
    实现seata-XA模式
    
### 1、修改application.yml文件（每个参与事务的微服务），开启XA模式：
    seata:  
      data-source-proxy-mode: XA # 开启数据源代理的XA模式
### 2、给发起全局事务的入口方法添加@GlobalTransactional注解，
    本例中是OrderServiceImpl中的create方法：
    
    @Override@GlobalTransactional
    public Long create(Order order) {    
        // 创建订单    
        orderMapper.insert(order);    
        // 扣余额 ...略 
        // 扣减库存 ...略    
        return order.getId();
    }
    
## AT
    实现seata-AT模式
### 1、修改application.yml文件（每个参与事务的微服务），开启AT模式：
    seata:  
      data-source-proxy-mode: AT # 开启数据源代理的AT模式
### 2、增加表
    seata 增加lock_table
    seata_demo增加undo_log
    
  
## TCC
### 1、新增业务表记录冻结金额
    account_freeze_tbl
### 2、实现AccountTCCService